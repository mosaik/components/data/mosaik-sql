from setuptools import setup


setup(
    name='mosaik-sql',
    version='0.1',
    author='Rami Elshinawy and Jan Sören Schwarz',
    author_email='mosaik at offis.de',
    description=('Stores mosaik simulation data in a SQL database.'),
    long_description=(open('README.md').read() + '\n\n' +
                      open('CHANGES.md').read() + '\n\n' +
                      open('AUTHORS.md').read()),
    url='https://gitlab.com/mosaik/components/data/mosaik-sql',
    install_requires=[
        'mosaik-api>=2.2',
        'mysql',
        'mysql-connector',
    ],
    py_modules=['mosaik_sql'],
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'mosaik-sql = mosaik_sql.mosaik:main',
        ],
    },
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU Lesser General Public License v2 (LGPLv2)',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Topic :: Scientific/Engineering',
    ],
)
