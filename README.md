mosaik-sql
==========

⚠ Warning: The code in this repository is not fully tested and unstable. Feel free to report any issues you experience with this code.

## Description
Stores mosaik simulation data in a SQL database.

## Installation

    $ pip install mosaik-sql

## Tests

You can run the tests with::

    $ git clone https://gitlab.com/mosaik/mosaik-sql.git
    $ cd mosaik-sql
    $ pip install -r requirements.txt
    $ pip install -e .
    $ py.test
    $ tox
