import mosaik
import dbCredentials
import time


sim_config = {
    'database': {
        'python': 'mosaik_sql:SQL'
        #'cmd': 'mosaik-sql'
    },
    'DB': {
        'cmd': 'mosaik-hdf5 %(addr)s',
    },
    'CSV': {
        'python': 'mosaik_csv:CSV',
    },
}

start_time_1 = time.perf_counter()
start_time_2 = time.process_time()

# Add PV data file.

END = 1 * 24 * 60 * 60 * 60  # one day in seconds
START = '2016-07-04 00:00:00'  # 'YYYY-MM-DD HH:mm:ss'
WEATHER_DATA = 'tests/data/weather_data.csv'
START_WEATHER_FORMAT = ''.join((START[:10], 'T', START[11:19], 'Z'))

# Set up the "world" of the scenario.

world = mosaik.World(sim_config)

# Initialize the simulators.

weathersim = world.start('CSV', sim_start=START_WEATHER_FORMAT,
                         datafile=WEATHER_DATA,
                         date_format='YYYY-MM-DDTHH:mm:ssZ')
database_sim = world.start('database',
                           step_size=60*60,
                           sim_start=START,
                           hostname=dbCredentials.hostname,
                           username=dbCredentials.username,
                           database=dbCredentials.database,
                           password=dbCredentials.password,
                           create_tables='single',
                           buf_size=0)
#db = world.start('DB', step_size=60*60, duration=END)

# Instantiate model entities.

temperature = weathersim.Weather()
database = database_sim.mosaik_sql.create(1)
#hdf5 = db.Database(filename='test.hdf5')

# Connections

world.connect(temperature, database[0], ('DE_temperature', 'outside_temperature'), 'DE_radiation_direct_horizontal',
              'DE_radiation_diffuse_horizontal')
#world.connect(temperature, hdf5, ('DE_temperature', 'outside_temperature'))

# Start simulation

world.run(until=END)

end_time_1 = time.perf_counter()
end_time_2 = time.process_time()

print('Time_1 : {}/n Time_2 : {}'.format((end_time_1-start_time_1), (end_time_2-start_time_2)))
